package com.szymonbilinski.toik.dto;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SudokuResponse {
    boolean wrong;
    ArrayList<Integer> lineIds;
    ArrayList<Integer> columnIds;
    ArrayList<Integer> areaIds;
}
