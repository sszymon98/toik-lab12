package com.szymonbilinski.toik.service;

import com.szymonbilinski.toik.dto.SudokuResponse;
import com.szymonbilinski.toik.utitles.CsvParser;
import com.szymonbilinski.toik.utitles.CsvParserImpl;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;

@Service
public class SudokuValidatorImpl implements SudokuValidator {
    private final CsvParser csvParser;
    private Integer[][] sudoku;

    ArrayList<Integer> lineIds = new ArrayList<>();
    ArrayList<Integer> columnIds = new ArrayList<>();
    ArrayList<Integer> areaIds = new ArrayList<>();

    public SudokuValidatorImpl(CsvParser csvParser) {
        this.csvParser = csvParser;
        sudoku = csvParser.getSudoku();
    }

    @Override
    public SudokuResponse isCorrect() throws IOException {
        lineIds.clear();
        areaIds.clear();
        columnIds.clear();

        for (int i=0;i<8;i++){
            for (int j=0;j<9;j++){
                System.out.print(sudoku[i][j]);
            }
            System.out.println();
        }
        // row checker
        for (int row = 0; row < 9; row++)
            for (int col = 0; col < 8; col++)
                for (int col2 = col + 1; col2 < 9; col2++)
                    if (sudoku[row][col] == sudoku[row][col2])
                        lineIds.add(col);

        // column checker
        for (int col = 0; col < 9; col++)
            for (int row = 0; row < 8; row++)
                for (int row2 = row + 1; row2 < 9; row2++)
                    if (sudoku[row][col] == sudoku[row2][col])
                        columnIds.add(col);

        // grid checker
        for (int row = 0; row < 9; row += 3)
            for (int col = 0; col < 9; col += 3)
                // row, col is start of the 3 by 3 grid
                for (int pos = 0; pos < 8; pos++)
                    for (int pos2 = pos + 1; pos2 < 9; pos2++)
                        if (sudoku[row + pos % 3][col + pos / 3] == sudoku[row + pos2 % 3][col + pos2 / 3])
                            areaIds.add(col);

        boolean status = lineIds.size() == 0 && columnIds.size() == 0 && areaIds.size() == 0;

        return new SudokuResponse(status, lineIds, columnIds, areaIds);

    }
}
