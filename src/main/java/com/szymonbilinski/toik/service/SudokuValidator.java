package com.szymonbilinski.toik.service;

import com.szymonbilinski.toik.dto.SudokuResponse;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.List;

public interface SudokuValidator {
    SudokuResponse isCorrect() throws IOException;
}
