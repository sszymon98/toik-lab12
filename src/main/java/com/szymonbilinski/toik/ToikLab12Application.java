package com.szymonbilinski.toik;

import com.szymonbilinski.toik.utitles.CsvParserImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class ToikLab12Application {

    public static void main(String[] args) {
        SpringApplication.run(ToikLab12Application.class, args);

    }

}
