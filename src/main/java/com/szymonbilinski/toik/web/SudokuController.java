package com.szymonbilinski.toik.web;

import com.szymonbilinski.toik.dto.SudokuResponse;
import com.szymonbilinski.toik.service.SudokuValidator;
import com.szymonbilinski.toik.service.SudokuValidatorImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import java.io.IOException;

@Controller
public class SudokuController {
    private final SudokuValidatorImpl sudokuValidator;

    public SudokuController(SudokuValidatorImpl sudokuValidator) {
        this.sudokuValidator = sudokuValidator;
    }

    @PostMapping("/api/sudoku/verify")
    public ResponseEntity<SudokuResponse> correct() throws IOException {
        SudokuResponse sudokuResponse = sudokuValidator.isCorrect();
        if (!sudokuResponse.isWrong()){
            return new ResponseEntity<>(sudokuResponse,HttpStatus.BAD_REQUEST);
        }else {
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }
}
