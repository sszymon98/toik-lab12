package com.szymonbilinski.toik.utitles;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface CsvParser {
    String PATH = "src/main/resources/sudoku.csv";
    void readCsvFile() throws IOException;
    Integer[][] getSudoku();
}
